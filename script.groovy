def buildApp(){
     echo 'Building the application'
     sh 'mvn package'
 }

def buildImage(){
     echo "Building the docker image"
    withCredentials([usernamePassword(credentialsId: 'docker_crendentials', usernameVariable: 'USER', passwordVariable: 'PWD')])
        {
          sh 'docker build -t aniket40/demoapp:jma-2.0 .'
          sh "echo $PWD | docker login -u $USER --password-stdin"
          sh 'docker push aniket40/demoapp:jma-2.0'
        }
}

def deployApp()
{
    echo "Deploying the application"
}

return this
